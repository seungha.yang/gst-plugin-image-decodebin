# 1. Prerequisites Setup

- Install git and python 3.6+
- Install meson and ninja
  - Install `meson` via Python's `pip` tool. This will also install `ninja`.
- Visual Studio 2017 or newer
- GStreamer 1.19.3, both runtime and development package
  - While installing, select "Custom" and ensure that gstreamer is installed into C:\gstreamer (or substitute the actual directory below)

# 2. Build

In short: run a terminal, add the gstreamer binaries to PATH, and run Meson.

Here we show how to do it in PowerShell:

``` sh
# Add GStreamer to PATH
PS C:\SomeWorkDir\gst-plugin-image-decodebin> $env:PATH += ";C:\gstreamer\1.0\msvc_x86_64\bin"

# Setup the builddir for the project
PS C:\SomeWorkDir\gst-plugin-image-decodebin> meson builddir

# Compile the project
PS C:\SomeWorkDir\gst-plugin-image-decodebin> meson compile -C builddir
```

