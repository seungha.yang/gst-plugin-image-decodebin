/* GStreamer
 * Copyright (C) 2022 Seungha Yang <seungha@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/video/video.h>
#include <gst/audio/audio.h>
#include <gst/pbutils/pbutils.h>

#include "gstimagedecodebin.h"
#include <string.h>

GST_DEBUG_CATEGORY_STATIC (gst_image_decode_bin_debug);
#define GST_CAT_DEFAULT gst_image_decode_bin_debug

static GstStaticPadTemplate video_template = GST_STATIC_PAD_TEMPLATE ("video",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate audio_template = GST_STATIC_PAD_TEMPLATE ("audio",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    GST_STATIC_CAPS_ANY);

typedef enum
{
  GST_IMAGE_DECODE_BIN_IMAGE_SYSTEM_MEM = 0,
  GST_IMAGE_DECODE_BIN_IMAGE_GL_MEM,
  GST_IMAGE_DECODE_BIN_IMAGE_D3D11_MEM,
} GstImageDecodeBinImageMemType;

static GType
gst_image_decode_bin_mem_type_get_type (void)
{
  static GType mem_type = 0;

  if (g_once_init_enter (&mem_type)) {
    static const GEnumValue values[] = {
      {GST_IMAGE_DECODE_BIN_IMAGE_SYSTEM_MEM, "System Memory", "system"},
      {GST_IMAGE_DECODE_BIN_IMAGE_GL_MEM, "GL Memory", "gl"},
      {GST_IMAGE_DECODE_BIN_IMAGE_D3D11_MEM, "D3D11 Memory", "d3d11"},
      {0,}
    };
    GType tmp = g_enum_register_static ("GstImageDecodeBinImageMemType",
        values);
    g_once_init_leave (&mem_type, tmp);
  }

  return mem_type;
}

enum
{
  PROP_0,
  PROP_IMAGE_URI,
  PROP_AUDIO_URI,
  PROP_DURATION,
  PROP_USE_AUDIO_DURATION,
  PROP_IMAGE_MEM_TYPE,
  PROP_RAW_IMAGE_CAPS,
  PROP_RAW_AUDIO_CAPS,
  PROP_IMAGE_FPS_N,
  PROP_IMAGE_FPS_D,
  PROP_SINGLE_IMAGE_BUFFER,
  PROP_LAST,
};

#define DEFAULT_DURATION GST_CLOCK_TIME_NONE

static GParamSpec *properties[PROP_LAST];

struct _GstImageDecodeBin
{
  GstBin parent;
  GstDiscoverer *discoverer;

  GMutex lock;
  gchar *image_uri;
  gchar *audio_uri;

  GstElement *image_dbin;
  GstElement *audio_dbin;

  gulong image_block_id;
  gulong audio_block_id;

  GstElement *image_process_bin;
  GstElement *audio_process_bin;

  GstPad *image_pad;
  GstPad *audio_pad;

  gboolean wait_for_image;
  gboolean wait_for_audio;

  gboolean exposed;
  GstClockTime audio_duration;
  GstClockTime target_duration;

  gboolean saw_image_buffer;

  /* properties */
  GstClockTime duration;
  gboolean use_audio_duration;
  GstImageDecodeBinImageMemType mem_type;
  GstCaps *raw_image_caps;
  GstCaps *raw_audio_caps;
  gboolean single_image_buffer;
  gint fps_n;
  gint fps_d;
};

static void gst_image_decode_bin_finalize (GObject * object);
static void gst_image_decode_bin_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static void gst_image_decode_bin_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static GstStateChangeReturn
gst_image_decode_bin_change_state (GstElement * element,
    GstStateChange transition);
static gboolean gst_image_decode_bin_query (GstElement * element,
    GstQuery * query);

#define gst_image_decode_bin_parent_class parent_class
G_DEFINE_TYPE (GstImageDecodeBin, gst_image_decode_bin, GST_TYPE_BIN);

static void
gst_image_decode_bin_class_init (GstImageDecodeBinClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  gobject_class->finalize = gst_image_decode_bin_finalize;
  gobject_class->get_property = gst_image_decode_bin_get_property;
  gobject_class->set_property = gst_image_decode_bin_set_property;

  properties[PROP_IMAGE_URI] =
      g_param_spec_string ("image-uri", "Image URI", "URI for image",
      NULL,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_AUDIO_URI] =
      g_param_spec_string ("audio-uri", "Audio URI", "URI for audio",
      NULL,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_DURATION] =
      g_param_spec_uint64 ("duration", "Duration", "Required stream duration",
      0, G_MAXUINT64, DEFAULT_DURATION,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_USE_AUDIO_DURATION] =
      g_param_spec_boolean ("use-audio-duration", "Use Audio Duration",
      "Use Audio duration", FALSE,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_USE_AUDIO_DURATION] =
      g_param_spec_boolean ("use-audio-duration", "Use Audio Duration",
      "Use Audio duration", FALSE,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_IMAGE_MEM_TYPE] =
      g_param_spec_enum ("image-memory-type", "Image Memory Type",
      "Image Memory Type", gst_image_decode_bin_mem_type_get_type (),
      GST_IMAGE_DECODE_BIN_IMAGE_SYSTEM_MEM,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_RAW_IMAGE_CAPS] =
      g_param_spec_boxed ("raw-image-caps", "Raw Image Caps",
      "Raw Image Caps", GST_TYPE_CAPS,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_RAW_AUDIO_CAPS] =
      g_param_spec_boxed ("raw-audio-caps", "Raw Audio Caps",
      "Raw Audio Caps", GST_TYPE_CAPS,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_IMAGE_FPS_N] =
      g_param_spec_int ("image-fps-n", "Image FPS N",
      "Image FPS N", 0, G_MAXINT32, 0,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_IMAGE_FPS_D] =
      g_param_spec_int ("image-fps-d", "Image FPS D",
      "Image FPS D", 0, G_MAXINT32, 0,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);
  properties[PROP_SINGLE_IMAGE_BUFFER] =
      g_param_spec_boolean ("single-image-buffer", "Single Image Buffer",
      "Push image buffer only once but with configured duration", FALSE,
      G_PARAM_READWRITE | GST_PARAM_MUTABLE_READY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (gobject_class, PROP_LAST, properties);

  element_class->change_state =
      GST_DEBUG_FUNCPTR (gst_image_decode_bin_change_state);
  element_class->query = GST_DEBUG_FUNCPTR (gst_image_decode_bin_query);

  gst_element_class_add_static_pad_template (element_class, &video_template);
  gst_element_class_add_static_pad_template (element_class, &audio_template);

  gst_element_class_set_static_metadata (element_class,
      "Image Decode Bin", "Generic/Bin/Decoder",
      "Image Decode Bin", "Seungha Yang <seungha@centricular.com>");

  GST_DEBUG_CATEGORY_INIT (gst_image_decode_bin_debug,
      "imagedecodebin", 0, "imagedecodebin");
}

static void
gst_image_decode_bin_init (GstImageDecodeBin * self)
{
  self->discoverer = gst_discoverer_new (10 * GST_SECOND, NULL);
  self->raw_image_caps = gst_caps_new_any ();
  self->raw_audio_caps = gst_caps_new_any ();
  self->duration = DEFAULT_DURATION;
  self->mem_type = GST_IMAGE_DECODE_BIN_IMAGE_SYSTEM_MEM;
  self->fps_n = 0;
  self->fps_d = 1;
  g_mutex_init (&self->lock);
}

static void
gst_image_decode_bin_finalize (GObject * object)
{
  GstImageDecodeBin *self = GST_IMAGE_DECODE_BIN (object);

  gst_clear_object (&self->discoverer);
  gst_clear_caps (&self->raw_image_caps);
  gst_clear_caps (&self->raw_audio_caps);

  g_free (self->image_uri);
  g_free (self->audio_uri);
  g_mutex_clear (&self->lock);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_image_decode_bin_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstImageDecodeBin *self = GST_IMAGE_DECODE_BIN (object);

  switch (prop_id) {
    case PROP_IMAGE_URI:
      g_value_set_string (value, self->image_uri);
      break;
    case PROP_AUDIO_URI:
      g_value_set_string (value, self->audio_uri);
      break;
    case PROP_DURATION:
      g_value_set_uint64 (value, self->duration);
      break;
    case PROP_USE_AUDIO_DURATION:
      g_value_set_boolean (value, self->use_audio_duration);
      break;
    case PROP_IMAGE_MEM_TYPE:
      g_value_set_enum (value, self->mem_type);
      break;
    case PROP_RAW_IMAGE_CAPS:
      gst_value_set_caps (value, self->raw_image_caps);
      break;
    case PROP_RAW_AUDIO_CAPS:
      gst_value_set_caps (value, self->raw_audio_caps);
      break;
    case PROP_IMAGE_FPS_N:
      g_value_set_int (value, self->fps_n);
      break;
    case PROP_IMAGE_FPS_D:
      g_value_set_int (value, self->fps_d);
      break;
    case PROP_SINGLE_IMAGE_BUFFER:
      g_value_set_boolean (value, self->single_image_buffer);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_image_decode_bin_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstImageDecodeBin *self = GST_IMAGE_DECODE_BIN (object);

  switch (prop_id) {
    case PROP_IMAGE_URI:
      g_free (self->image_uri);
      self->image_uri = g_value_dup_string (value);
      break;
    case PROP_AUDIO_URI:
      g_free (self->audio_uri);
      self->audio_uri = g_value_dup_string (value);
      break;
    case PROP_DURATION:
      self->duration = g_value_get_uint64 (value);
      break;
    case PROP_USE_AUDIO_DURATION:
      self->use_audio_duration = g_value_get_boolean (value);
      break;
    case PROP_IMAGE_MEM_TYPE:
      self->mem_type = g_value_get_enum (value);
      break;
    case PROP_RAW_IMAGE_CAPS:
    {
      GstCaps *new_caps;
      const GstCaps *new_caps_val = gst_value_get_caps (value);

      if (new_caps_val == NULL) {
        new_caps = gst_caps_new_any ();
      } else {
        new_caps = (GstCaps *) new_caps_val;
        gst_caps_ref (new_caps);
      }

      gst_clear_caps (&self->raw_image_caps);
      self->raw_image_caps = new_caps;
      break;
    }
    case PROP_RAW_AUDIO_CAPS:
    {
      GstCaps *new_caps;
      const GstCaps *new_caps_val = gst_value_get_caps (value);

      if (new_caps_val == NULL) {
        new_caps = gst_caps_new_any ();
      } else {
        new_caps = (GstCaps *) new_caps_val;
        gst_caps_ref (new_caps);
      }

      gst_clear_caps (&self->raw_audio_caps);
      self->raw_audio_caps = new_caps;
      break;
    }
    case PROP_IMAGE_FPS_N:
      self->fps_n = g_value_get_int (value);
      break;
    case PROP_IMAGE_FPS_D:
      self->fps_d = g_value_get_int (value);
      break;
    case PROP_SINGLE_IMAGE_BUFFER:
      self->single_image_buffer = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static GstPadProbeReturn
pad_block_cb (GstPad * pad, GstPadProbeInfo * info, GstImageDecodeBin * self)
{
  GST_DEBUG_OBJECT (self, "Blocking pad (%s:%s)", GST_DEBUG_PAD_NAME (pad));

  return GST_PAD_PROBE_OK;
}

static GstPadProbeReturn
src_query_probe (GstPad * pad, GstPadProbeInfo * info, GstImageDecodeBin * self)
{
  GstPadProbeReturn ret = GST_PAD_PROBE_OK;

  if (GST_IS_QUERY (GST_PAD_PROBE_INFO_DATA (info))) {
    GstQuery *query = GST_PAD_PROBE_INFO_QUERY (info);

    switch (GST_QUERY_TYPE (query)) {
      case GST_QUERY_DURATION:
        if (GST_CLOCK_TIME_IS_VALID (self->target_duration)) {
          gst_query_set_duration (query,
              GST_FORMAT_TIME, self->target_duration);
          ret = GST_PAD_PROBE_HANDLED;
        }
        break;
      default:
        break;
    }
  }

  return ret;
}

static GstPadProbeReturn
src_buffer_probe (GstPad * pad, GstPadProbeInfo * info,
    GstImageDecodeBin * self)
{
  GstPadProbeReturn ret = GST_PAD_PROBE_OK;

  if (GST_IS_BUFFER (GST_PAD_PROBE_INFO_DATA (info)) &&
      GST_CLOCK_TIME_IS_VALID (self->target_duration)) {
    GstEvent *event = gst_pad_get_sticky_event (pad, GST_EVENT_SEGMENT, 0);
    GstBuffer *buf = GST_PAD_PROBE_INFO_BUFFER (info);
    GstClockTime pts;
    GstClockTime running_time;
    const GstSegment *segment;

    if (!event)
      return GST_PAD_PROBE_OK;

    gst_event_parse_segment (event, &segment);
    gst_event_unref (event);

    pts = GST_BUFFER_PTS (buf);
    if (!GST_CLOCK_TIME_IS_VALID (pts))
      return GST_PAD_PROBE_OK;

    running_time = gst_segment_to_running_time (segment, GST_FORMAT_TIME, pts);

    if (!GST_CLOCK_TIME_IS_VALID (running_time))
      return GST_PAD_PROBE_OK;

    if (GST_BUFFER_DURATION_IS_VALID (buf))
      running_time += GST_BUFFER_DURATION (buf);

    if (running_time > self->target_duration) {
      GST_DEBUG_OBJECT (self, "Out of duration");
      gst_buffer_unref (buf);
      GST_PAD_PROBE_INFO_FLOW_RETURN (info) = GST_FLOW_EOS;
      ret = GST_PAD_PROBE_HANDLED;
    }
  }

  return ret;
}

static void
expose_pad (GstImageDecodeBin * self, GstPad * pad, gboolean do_buffer)
{
  gst_pad_set_active (pad, TRUE);
  gst_element_add_pad (GST_ELEMENT (self), pad);

  gst_pad_add_probe (pad,
      GST_PAD_PROBE_TYPE_QUERY_BOTH,
      (GstPadProbeCallback) src_query_probe, self, NULL);


  if (do_buffer) {
    gst_pad_add_probe (pad,
        GST_PAD_PROBE_TYPE_BUFFER,
        (GstPadProbeCallback) src_buffer_probe, self, NULL);
  }
}

static void
expose_all_pads (GstImageDecodeBin * self)
{
  GstPad *pad;

  if (self->exposed)
    return;

  if (self->wait_for_image || self->wait_for_audio)
    return;

  if (!self->image_process_bin) {
    g_mutex_unlock (&self->lock);
    GST_ELEMENT_ERROR (self,
        RESOURCE, NOT_FOUND, ("No image to output"), (NULL));
    g_mutex_unlock (&self->lock);
    return;
  }

  self->exposed = TRUE;

  g_mutex_unlock (&self->lock);

  if (self->use_audio_duration && self->audio_process_bin &&
      GST_CLOCK_TIME_IS_VALID (self->audio_duration)) {
    self->target_duration = self->audio_duration;
  } else if (GST_CLOCK_TIME_IS_VALID (self->duration)) {
    self->target_duration = self->duration;
  } else {
    self->target_duration = GST_CLOCK_TIME_NONE;
  }

  GST_DEBUG_OBJECT (self, "Target duration %" GST_TIME_FORMAT,
      GST_TIME_ARGS (self->target_duration));

  pad = gst_element_get_static_pad (self->image_process_bin, "src");
  self->image_pad = gst_ghost_pad_new ("video", pad);
  gst_object_unref (pad);
  expose_pad (self, self->image_pad, !self->single_image_buffer);

  if (self->audio_process_bin) {
    pad = gst_element_get_static_pad (self->audio_process_bin, "src");
    self->audio_pad = gst_ghost_pad_new ("audio", pad);
    gst_object_unref (pad);

    expose_pad (self, self->audio_pad, TRUE);
  }

  GST_INFO_OBJECT (self, "Posting no more pads");
  gst_element_no_more_pads (GST_ELEMENT (self));

  g_mutex_lock (&self->lock);
  if (self->image_process_bin && self->image_block_id) {
    pad = gst_element_get_static_pad (self->image_process_bin, "sink");
    gst_pad_remove_probe (pad, self->image_block_id);
    gst_object_unref (pad);
  }

  if (self->audio_process_bin && self->audio_block_id) {
    pad = gst_element_get_static_pad (self->audio_process_bin, "sink");
    gst_pad_remove_probe (pad, self->audio_block_id);
    gst_object_unref (pad);
  }

  self->image_block_id = 0;
  self->audio_block_id = 0;
}

static GstPadProbeReturn
image_buffer_drop_probe (GstPad * pad, GstPadProbeInfo * info,
    GstImageDecodeBin * self)
{
  GstPadProbeReturn ret = GST_PAD_PROBE_OK;

  if (GST_IS_BUFFER (GST_PAD_PROBE_INFO_DATA (info)) &&
      GST_CLOCK_TIME_IS_VALID (self->target_duration) &&
      self->single_image_buffer) {
    GstBuffer *buf = GST_PAD_PROBE_INFO_BUFFER (info);

    if (self->saw_image_buffer) {
      return GST_PAD_PROBE_DROP;
    } else if (GST_BUFFER_PTS_IS_VALID (buf)) {
      GstBuffer *buf = GST_PAD_PROBE_INFO_BUFFER (info);
      GstBuffer *new_buf = gst_buffer_copy (buf);
      GstPad *peer = gst_pad_get_peer (pad);

      GST_INFO_OBJECT (self, "Got first image buffer %" GST_PTR_FORMAT, buf);

      GST_BUFFER_DURATION (new_buf) = self->target_duration;

      GST_INFO_OBJECT (self, "Pushing image buffer %" GST_PTR_FORMAT, new_buf);

      gst_pad_chain (peer, new_buf);

      gst_pad_send_event (peer, gst_event_new_eos ());
      gst_object_unref (peer);
      gst_buffer_unref (buf);
      self->saw_image_buffer = TRUE;
      GST_PAD_PROBE_INFO_FLOW_RETURN (info) = GST_FLOW_EOS;

      return GST_PAD_PROBE_HANDLED;
    }
  }

  return ret;
}

static GstElement *
create_image_process_bin (GstImageDecodeBin * self)
{
  GstElement *bin = gst_bin_new ("image-process-bin");
  GstElement *videoconvert = gst_element_factory_make ("videoconvert", NULL);
  GstElement *videoscale = gst_element_factory_make ("videoscale", NULL);
  GstElement *capsfilter = gst_element_factory_make ("capsfilter", NULL);
  GstElement *upload_capsfilter = gst_element_factory_make ("capsfilter", NULL);
  GstElement *imagefreeze = NULL;
  GstElement *imagefreeze_capsfilter = NULL;
  GstPad *pad, *gpad;
  GstCaps *caps = NULL;
  GstElement *upload = NULL;
  GstElement *end_elem;

  switch (self->mem_type) {
    case GST_IMAGE_DECODE_BIN_IMAGE_GL_MEM:
      caps = gst_caps_from_string ("video/x-raw(memory:GLMemory)");
      upload = gst_element_factory_make ("glupload", NULL);
      break;
    case GST_IMAGE_DECODE_BIN_IMAGE_D3D11_MEM:
      caps = gst_caps_from_string ("video/x-raw(memory:D3D11Memory)");
      upload = gst_element_factory_make ("d3d11upload", NULL);
      break;
    default:
      caps = gst_caps_from_string ("video/x-raw");
      upload = gst_element_factory_make ("identity", NULL);
      break;
  }

  if (self->raw_image_caps && !gst_caps_is_any (self->raw_image_caps))
    g_object_set (capsfilter, "caps", self->raw_image_caps, NULL);

  g_object_set (upload_capsfilter, "caps", caps, NULL);

  gst_bin_add_many (GST_BIN (bin), videoconvert, videoscale, capsfilter,
      upload, upload_capsfilter, NULL);
  gst_element_link_many (videoconvert, videoscale, capsfilter, upload,
      upload_capsfilter, NULL);
#if 0
  if (self->single_image_buffer) {
    GstElement *videorate = gst_element_factory_make ("videorate", NULL);
    GstElement *rate_capsfilter = gst_element_factory_make ("capsfilter", NULL);
    GstCaps *rate_caps = gst_caps_copy (caps);

    gst_caps_set_simple (rate_caps, "framerate", GST_TYPE_FRACTION, 1, 1, NULL);
    g_object_set (rate_capsfilter, "caps", rate_caps, NULL);

    gst_bin_add_many (GST_BIN (bin), videorate, rate_capsfilter, NULL);
    gst_element_link_many (upload_capsfilter, videorate, rate_capsfilter, NULL);

    pad = gst_element_get_static_pad (capsfilter, "src");
    gst_pad_add_probe (pad, GST_PAD_PROBE_TYPE_BUFFER,
        (GstPadProbeCallback) image_buffer_drop_probe, self, NULL);
    gst_object_unref (pad);

    end_elem = rate_capsfilter;
  } else
#endif
  {
    imagefreeze = gst_element_factory_make ("imagefreeze", NULL);
    imagefreeze_capsfilter = gst_element_factory_make ("capsfilter", NULL);

    caps = gst_caps_make_writable (caps);
    gst_caps_set_simple (caps, "framerate", GST_TYPE_FRACTION, 30, 1, NULL);
    g_object_set (imagefreeze_capsfilter, "caps", caps, NULL);

    gst_bin_add_many (GST_BIN (bin), imagefreeze, imagefreeze_capsfilter, NULL);
    gst_element_link_many (upload_capsfilter,
        imagefreeze, imagefreeze_capsfilter, NULL);
    end_elem = imagefreeze_capsfilter;
  }
  gst_clear_caps (&caps);

  pad = gst_element_get_static_pad (videoconvert, "sink");
  gpad = gst_ghost_pad_new ("sink", pad);
  gst_object_unref (pad);

  gst_element_add_pad (bin, gpad);

  pad = gst_element_get_static_pad (end_elem, "src");
  gpad = gst_ghost_pad_new ("src", pad);
  gst_object_unref (pad);

  gst_element_add_pad (bin, gpad);

  return bin;
}

static void
on_image_dbin_pad_added (GstElement * dbin, GstPad * pad,
    GstImageDecodeBin * self)
{
  GstCaps *caps;
  const gchar *name;
  GstPad *otherpad;

  if (self->image_block_id) {
    GST_INFO_OBJECT (self, "Has image pad already");
    return;
  }

  caps = gst_pad_get_current_caps (pad);
  if (!caps) {
    GST_ERROR_OBJECT (self, "pad holds no caps");
    return;
  }

  name = gst_structure_get_name (gst_caps_get_structure (caps, 0));

  if (!g_str_has_prefix (name, "video")) {
    GST_DEBUG_OBJECT (pad, "Not a image pad");
    gst_caps_unref (caps);
    return;
  }

  gst_caps_unref (caps);

  self->image_process_bin = create_image_process_bin (self);
  gst_bin_add (GST_BIN (self), self->image_process_bin);
  gst_element_sync_state_with_parent (self->image_process_bin);

  otherpad = gst_element_get_static_pad (self->image_process_bin, "src");
  gst_pad_set_active (otherpad, TRUE);
  gst_object_unref (otherpad);

  otherpad = gst_element_get_static_pad (self->image_process_bin, "sink");
  gst_pad_set_active (otherpad, TRUE);
  gst_pad_link (pad, otherpad);

  self->image_block_id = gst_pad_add_probe (otherpad,
      GST_PAD_PROBE_TYPE_BLOCK | GST_PAD_PROBE_TYPE_DATA_DOWNSTREAM,
      (GstPadProbeCallback) pad_block_cb, self, NULL);
  gst_object_unref (otherpad);
}

static void
on_image_dbin_no_more_pads (GstElement * dbin, GstImageDecodeBin * self)
{
  GST_INFO_OBJECT (self, "No more pads from %" GST_PTR_FORMAT, dbin);

  g_mutex_lock (&self->lock);
  if (self->wait_for_image) {
    self->wait_for_image = FALSE;
    expose_all_pads (self);
  }
  g_mutex_unlock (&self->lock);
}

static GstElement *
create_audio_process_bin (GstImageDecodeBin * self)
{
  GstElement *bin = gst_bin_new ("audio-process-bin");
  GstElement *audioconvert = gst_element_factory_make ("audioconvert", NULL);
  GstElement *audioresample = gst_element_factory_make ("audioresample", NULL);
  GstElement *capsfilter = gst_element_factory_make ("capsfilter", NULL);
  GstPad *pad, *gpad;

  gst_bin_add_many (GST_BIN (bin),
      audioconvert, audioresample, capsfilter, NULL);
  gst_element_link_many (audioconvert, audioresample, capsfilter, NULL);

  if (self->raw_audio_caps && !gst_caps_is_any (self->raw_audio_caps)) {
    g_object_set (capsfilter, "caps", self->raw_audio_caps, NULL);
  }

  pad = gst_element_get_static_pad (audioconvert, "sink");
  gpad = gst_ghost_pad_new ("sink", pad);
  gst_object_unref (pad);

  gst_element_add_pad (bin, gpad);

  pad = gst_element_get_static_pad (capsfilter, "src");
  gpad = gst_ghost_pad_new ("src", pad);
  gst_object_unref (pad);

  gst_element_add_pad (bin, gpad);

  return bin;
}

static void
on_audio_dbin_pad_added (GstElement * dbin, GstPad * pad,
    GstImageDecodeBin * self)
{
  GstCaps *caps;
  const gchar *name;
  GstPad *otherpad;
  GstClockTime dur = GST_CLOCK_TIME_NONE;

  if (self->audio_block_id) {
    GST_INFO_OBJECT (self, "Has audio pad already");
    return;
  }

  caps = gst_pad_get_current_caps (pad);
  if (!caps) {
    GST_ERROR_OBJECT (self, "pad holds no caps");
    return;
  }

  name = gst_structure_get_name (gst_caps_get_structure (caps, 0));
  if (!g_str_has_prefix (name, "audio")) {
    GST_DEBUG_OBJECT (pad, "Not a audio pad");
    gst_caps_unref (caps);
    return;
  }

  gst_caps_unref (caps);

  if (gst_element_query_duration (dbin, GST_FORMAT_TIME, &dur) &&
      GST_CLOCK_TIME_IS_VALID (dur) && dur > 0) {
    GST_INFO_OBJECT (self,
        "Audio duration %" GST_TIME_FORMAT, GST_TIME_ARGS (dur));
    self->audio_duration = dur;
  } else if (GST_CLOCK_TIME_IS_VALID (self->audio_duration) &&
      self->audio_duration > 0) {
    GST_INFO_OBJECT (self, "Use cached audio duration %" GST_TIME_FORMAT,
        GST_TIME_ARGS (self->audio_duration));
  } else {
    GST_WARNING_OBJECT (self, "Unknown audio duration");
  }

  self->audio_process_bin = create_audio_process_bin (self);
  gst_bin_add (GST_BIN (self), self->audio_process_bin);
  gst_element_sync_state_with_parent (self->audio_process_bin);

  otherpad = gst_element_get_static_pad (self->audio_process_bin, "src");
  gst_pad_set_active (otherpad, TRUE);
  gst_object_unref (otherpad);

  otherpad = gst_element_get_static_pad (self->audio_process_bin, "sink");
  gst_pad_set_active (otherpad, TRUE);
  gst_pad_link (pad, otherpad);

  self->audio_block_id = gst_pad_add_probe (otherpad,
      GST_PAD_PROBE_TYPE_BLOCK | GST_PAD_PROBE_TYPE_DATA_DOWNSTREAM,
      (GstPadProbeCallback) pad_block_cb, self, NULL);

  gst_object_unref (otherpad);
}

static void
on_audio_dbin_no_more_pads (GstElement * dbin, GstImageDecodeBin * self)
{
  GST_INFO_OBJECT (self, "No more pads from %" GST_PTR_FORMAT, dbin);

  g_mutex_lock (&self->lock);
  if (self->wait_for_audio) {
    self->wait_for_audio = FALSE;
    expose_all_pads (self);
  }
  g_mutex_unlock (&self->lock);
}

static gboolean
create_decodebin (GstImageDecodeBin * self, const gchar * uri,
    gboolean is_image)
{
  GstDiscovererInfo *info;
  GstElement *source;
  GError *err = NULL;
  gboolean has_stream = FALSE;
  GList *streams, *iter;
  GstClockTime duration = GST_CLOCK_TIME_NONE;

  GST_DEBUG_OBJECT (self, "Creating decodebin, image: %d, uri: %s",
      is_image, uri);

  info = gst_discoverer_discover_uri (self->discoverer, uri, &err);
  if (!info) {
    GST_ERROR_OBJECT (self, "Failed to discover uri %s, %s", uri,
        GST_STR_NULL (err->message));
    g_clear_error (&err);
    return FALSE;
  }

  duration = gst_discoverer_info_get_duration (info);

  GST_DEBUG_OBJECT (self, "Discorvored duration %" GST_TIME_FORMAT,
      GST_TIME_ARGS (duration));

  streams = gst_discoverer_info_get_stream_list (info);
  gst_discoverer_stream_info_unref (info);
  /* Pick the first video stream */
  for (iter = streams; iter; iter = g_list_next (iter)) {
    if (is_image && GST_IS_DISCOVERER_VIDEO_INFO (iter->data)) {
      has_stream = TRUE;
      break;
    } else if (!is_image && GST_IS_DISCOVERER_AUDIO_INFO (iter->data)) {
      has_stream = TRUE;
      break;
    }
  }

  gst_discoverer_stream_info_list_free (streams);
  if (!has_stream) {
    GST_ERROR_OBJECT (self, "uri \"%s\" doesn't contain %s stream",
        uri, is_image ? "image" : "audio");
    return FALSE;
  }

  if (is_image) {
    self->wait_for_image = TRUE;
    self->image_dbin = source =
        gst_element_factory_make ("uridecodebin", "image-decodebin");
    g_signal_connect (source, "pad-added",
        G_CALLBACK (on_image_dbin_pad_added), self);
    g_signal_connect (source, "no-more-pads",
        G_CALLBACK (on_image_dbin_no_more_pads), self);
  } else {
    self->wait_for_audio = TRUE;
    self->audio_duration = duration;
    self->audio_dbin = source =
        gst_element_factory_make ("uridecodebin", "audio-decodebin");
    g_signal_connect (source, "pad-added",
        G_CALLBACK (on_audio_dbin_pad_added), self);
    g_signal_connect (source, "no-more-pads",
        G_CALLBACK (on_audio_dbin_no_more_pads), self);
  }

  g_object_set (source, "uri", uri, NULL);

  gst_bin_add (GST_BIN (self), source);
  gst_element_sync_state_with_parent (source);

  return TRUE;
}

static GstStateChangeReturn
gst_image_decode_bin_change_state (GstElement * element,
    GstStateChange transition)
{
  GstImageDecodeBin *self = GST_IMAGE_DECODE_BIN (element);
  GstStateChangeReturn ret;

  GST_LOG_OBJECT (self, "Begin transition %s",
      gst_state_change_get_name (transition));

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      if (!self->image_uri || !gst_uri_is_valid (self->image_uri)) {
        GST_ELEMENT_ERROR (self, RESOURCE, NOT_FOUND,
            ("Invalid image uri %s", GST_STR_NULL (self->image_uri)), (NULL));
        return GST_STATE_CHANGE_FAILURE;
      }

      create_decodebin (self, self->image_uri, TRUE);
      if (self->audio_uri) {
        if (!gst_uri_is_valid (self->audio_uri)) {
          GST_WARNING_OBJECT (self, "Invalid audio uri %s", self->audio_uri);
        } else {
          create_decodebin (self, self->audio_uri, FALSE);
        }
      }
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_NULL:
      if (self->image_process_bin) {
        gst_pad_set_active (self->image_pad, FALSE);
        gst_ghost_pad_set_target (GST_GHOST_PAD (self->image_pad), NULL);
        gst_element_remove_pad (GST_ELEMENT (self), self->image_pad);
        self->image_pad = NULL;
        gst_bin_remove (GST_BIN (self), self->image_process_bin);
        self->image_process_bin = NULL;
        self->image_block_id = 0;
      }

      if (self->audio_process_bin) {
        gst_pad_set_active (self->audio_pad, FALSE);
        gst_ghost_pad_set_target (GST_GHOST_PAD (self->audio_pad), NULL);
        gst_element_remove_pad (GST_ELEMENT (self), self->audio_pad);
        self->audio_pad = NULL;
        gst_bin_remove (GST_BIN (self), self->audio_process_bin);
        self->audio_process_bin = NULL;
        self->audio_block_id = 0;
      }

      self->wait_for_image = FALSE;
      self->wait_for_audio = FALSE;
      self->exposed = FALSE;
      self->audio_duration = GST_CLOCK_TIME_NONE;
      self->target_duration = GST_CLOCK_TIME_NONE;
      self->saw_image_buffer = FALSE;
      self->image_pad = NULL;
      self->audio_pad = NULL;
      break;
    default:
      break;
  }

  GST_LOG_OBJECT (self, "End transition %s",
      gst_state_change_get_name (transition));

  return ret;
}

static gboolean
gst_image_decode_bin_query (GstElement * element, GstQuery * query)
{
  GstImageDecodeBin *self = GST_IMAGE_DECODE_BIN (element);

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_DURATION:
    {
      gboolean forward = TRUE;
      g_mutex_lock (&self->lock);
      if (self->exposed && GST_CLOCK_TIME_IS_VALID (self->target_duration)) {
        gst_query_set_duration (query, GST_FORMAT_TIME, self->target_duration);
        forward = FALSE;
      }
      g_mutex_unlock (&self->lock);

      if (!forward)
        return TRUE;

      break;
    }
    default:
      break;
  }

  return GST_ELEMENT_CLASS (parent_class)->query (element, query);
}
